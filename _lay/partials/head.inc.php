<!doctype html>
<html lang="pl" prefix="og: http://ogp.me/ns#">
	<head>

		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
		<meta name="format-detection" content="telephone=no">

		<title><?php echo $page['title']; ?></title>
		<meta name="description" content="<?php echo $page['description']; ?>">
		<meta name="keywords" content="<?php echo $page['keywords']; ?>">

		<link rel="stylesheet" type="text/css" href="assets/css/app.css">
		<link rel="stylesheet" type="text/css" href="assets/css/jquery.fancybox.min.css">
		<link rel="stylesheet" type="text/css" href="assets/css/jquery.mCustomScrollbar.css">

		<link rel="stylesheet" type="text/css" href="assets/css/owl.carousel.css">
		<link rel="stylesheet" type="text/css" href="assets/css/owl.theme.default.css">
		<link rel="stylesheet" type="text/css" href="assets/css/jquery-ui.css">

	</head>
	<body>