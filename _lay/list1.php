<?php

	$page = array(
		'title' => 'index3',
		'description' => '',
		'keywords' => '',
		'bodyclass' => '',
	);

?>

<?php include 'partials/head.inc.php'; ?>
<?php include 'partials/header.inc.php'; ?>

<div class="wrapper">

	<div class="content content--wide">

		<div class="content__header"></div>

		<div class="box--relative box-heightfull flat-list flat-list1 flat-list-full">

			<div class="owl-carousel owl-theme owl-mbox">

			<?php for($i=0; $i <= 3; $i++): ?>
				
				<div class="item flat-list-item">

					<div class="mbox__item flat-list-item-container">

						<div class="text--center">

							<div class="header--cage">Budynek B</div>
							<div class="header--flat">Mieszkanie AA432</div>

							<img src="tmp/flat-view-410x223.png">

						</div> <!-- .text -->

						<table class="table-flat table-flat--centered">
							<tbody>

								<tr>
									<td class="td--bold">Powierzchnia</td>
									<td class="td--bold">65 m2</td>
								</tr>

								<tr>
									<td>Liczba pokoi</td>
									<td class="td--bold">4</td>
								</tr>

								<tr>
									<td>Piętro</td>
									<td class="td--bold">2</td>
								</tr>

							</tbody>
						</table>

						<hr class="hr-default hr-default--centeted">

						<div class="prices">

							<div class="price__title">Cena brutto</div>
							<div class="price__value">873 245,00 zł</div>
							<div class="price__value-m">Cena za m2 <span class="font--bold">10 250,00 zł</span></div>

						</div> <!-- .prices -->

					</div> <!-- .mbox-item -->

					<a class="item-unselect">
						<svg class="icon-svg icon-svg-close" viewbox="0 0 40 40"><path class="close-x" d="M 10,10 L 30,30 M 30,10 L 10,30" /></svg>
					</a>

				</div> <!-- .item -->

			<?php endfor; ?>


			</div> <!-- .owl-carousel -->

			<div class="content__footer text--center">

				<a href="#" class="btn btn--tbb btn--260"><img class="ico-compare" src="assets/images/ico-compare.png"> Porównaj</a>

				<a href="#" class="btn btn--260"><img class="ico-send" src="assets/images/ico-send.png">Wyślij</a>

			</div> <!-- .content__footerr -->

		</div> <!-- box -- >

	</div> <!-- .content -->

</div> <!-- .wrapper -->

<?php include 'partials/footer.inc.php'; ?>
<?php include 'partials/foot.inc.php';