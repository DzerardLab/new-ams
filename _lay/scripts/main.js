$( function() {

	// custom scroll

	$(window).on("load",function(){
		$(".gallery__content").mCustomScrollbar({
			documentTouchScroll: true
		});
	});


	// rage slider
	var filterAreaMin = $('.filter-area-range').data('filter-area-min');
	var filterAreaMax = $('.filter-area-range').data('filter-area-max');

	$('.filter-area-range').slider({
			range: true,
			min: filterAreaMin,
			max: filterAreaMax,
			values: [$('.js-area-min').text(), $('.js-area-max').text()],
			slide: function (event, ui) {
				$('.js-area-min').text(ui.values[0]);
				$('.js-area-max').text(ui.values[1]);
			}
	});

	// rage slider
	var filterPriceMin = $('.filter-price-range').data('filter-price-min');
	var filterPriceMax = $('.filter-price-range').data('filter-price-max');

	$('.filter-price-range').slider({
			range: true,
			min: filterPriceMin,
			max: filterPriceMax,
			values: [$('.js-price-min').text(), $('.js-price-max').text()],
			slide: function (event, ui) {
				$('.js-price-min').text(ui.values[0]);
				$('.js-price-max').text(ui.values[1]);
			},
	});


	// owl
	$(".owl-mbox").owlCarousel({
		loop: false,
		margin: 110,
		nav: false,
		items: 3,
		dots: false,
	});

	$(".owl-cbox").owlCarousel({
		loop: false,
		margin: 100,
		nav: false,
		items: 3,
	});


	// select flat
	$('.flat-list1 .flat-list-item-container').click(function(){

		if($(this).hasClass('mbox__item--active')) {
			$(this).removeClass('mbox__item--active');
		} else {
			if ($('.flat-list1 .flat-list-item-container.mbox__item--active').length<3) {
				$(this).addClass('mbox__item--active');
			}
		}
	});

	$('.flat-list2 .flat-list-item-container').click(function(){

		if($(this).hasClass('mbox__item--active')) {
			$(this).removeClass('mbox__item--active');
		} else {
			$(".flat-list .flat-list-item-container").removeClass('mbox__item--active');
			$(this).addClass('mbox__item--active');
		}
	});
	
	$('.flat-list-item .item-unselect').click(function(){
		$(this).parent().children(".flat-list-item-container").removeClass('mbox__item--active');
	});


	$(window).click(function() {
		$('.box-step__list').removeClass('is_active');
	});

	$('.box-step__change').click(function(event){
		$(this).next().toggleClass('is_active');
		event.stopPropagation();
	});

	$('.box-step__list').click(function(event){
		event.stopPropagation();
	});


	// swipe floors
	var count = 0;

	$(".box__flors__buttons-inner").swipe( {

		swipeDown:function(event, direction, distance, duration, fingerCount) {

			var view = $('.box__flors__buttons-inner');
			var sliderLimit = '0';
			var move = '487px';
			var currentPosition = parseInt(view.css("top"));

			if (currentPosition >= sliderLimit) view.stop(false,true).animate({top:"-="+move},{ duration: 400})

			$('[data-floor-st]').show();
			$('[data-floor-sb]').hide();

		},
		swipeUp:function(event, direction, distance, duration, fingerCount) {

			var view = $('.box__flors__buttons-inner');
			var sliderLimit = '0';
			var move = '445px';
			var currentPosition = parseInt(view.css("top"));

			if (currentPosition < 0) view.stop(false,true).animate({top:"+="+move},{ duration: 400})

			$('[data-floor-st]').hide();
			$('[data-floor-sb]').show();

		}
	});

	$("[data-fancybox]").fancybox();

	$('.js-price-mask').mask('000 000 000', {reverse: true});

});


// switch floors
var view = $('.box__flors__buttons-inner');
var move = '445px';
var sliderLimit = 0

$('[data-floor-sb]').click(function(){

	var currentPosition = parseInt(view.css("top"));

	if (currentPosition >= sliderLimit) view.stop(false,true).animate({top:"-="+move},{ duration: 400})
	$('[data-floor-st]').show();
	$(this).hide();

});

$('[data-floor-st]').click(function(){

	var currentPosition = parseInt(view.css("top"));

	if (currentPosition < 0) view.stop(false,true).animate({top:"+="+move},{ duration: 400})
	$('[data-floor-sb]').show();
	$(this).hide();

});


// show / hide row filtration
$('[data-filtration-row]').click(function(){

	$('[data-filtration-row-hover]').hide();
	$('[data-filtration-row]').show();

	$(this).toggle();
	$(this).next().toggle();
});
$('[data-filtration-row-hover]').click(function(){
	$(this).toggle();
	$(this).prev().toggle();
});