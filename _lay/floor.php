<?php

	$page = array(
		'title' => 'index2',
		'description' => '',
		'keywords' => '',
		'bodyclass' => '',
	);

?>

<?php include 'partials/head.inc.php'; ?>
<?php include 'partials/header.inc.php'; ?>

<div class="wrapper">

	<div class="content content--wide">

		<div class="content__header">

			<a href="#" class="btn btn--tbr btn--back">Wróć</a>

			<div class="box-step">
				<div class="box-step__head">Etap <strong>B</strong></div>
				<div class="box-step__change">[ zmień ]</div>
				<ul class="box-step__list">
					<li><a href="#">Etap B</a></li>
					<li><a href="#">Etap C</a></li>
				</ul> <!-- .box-step__list -->
			</div> <!-- .box-step -->

		</div> <!-- .content__header -->

		<div class="box--relative box-heightfull">

			<div class="content__floors">

				<div class="content__floors__title">
					Piętra
				</div> <!-- .content__floors__title -->

				<div class="box__flors__buttons">

					<div class="box__flors__buttons-inner">

						<div class="btn__row">
							<a href="#" class="btn btn--s btn--blue btn--w100 btn--active">1</a>
						</div> <!-- .btn__row -->

						<div class="btn__row">
							<a href="#" class="btn btn--s btn--blue btn--w100">2</a>
						</div> <!-- .btn__row -->

						<div class="btn__row">
							<a href="#" class="btn btn--s btn--blue btn--w100">3</a>
						</div> <!-- .btn__row -->

						<div class="btn__row">
							<a href="#" class="btn btn--s btn--blue btn--w100">4</a>
						</div> <!-- .btn__row -->

						<div class="btn__row">
							<a href="#" class="btn btn--s btn--blue btn--w100">5</a>
						</div> <!-- .btn__row -->

						<div class="btn__row">
							<a href="#" class="btn btn--s btn--blue btn--w100">6</a>
						</div> <!-- .btn__row -->

						<div class="btn__row">
							<a href="#" class="btn btn--s btn--blue btn--w100">7</a>
						</div> <!-- .btn__row -->

						<div class="btn__row">
							<a href="#" class="btn btn--s btn--blue btn--w100">8</a>
						</div> <!-- .btn__row -->

						<div class="btn__row">
							<a href="#" class="btn btn--s btn--blue btn--w100">9</a>
						</div> <!-- .btn__row -->

						<div class="btn__row">
							<a href="#" class="btn btn--s btn--blue btn--w100">10</a>
						</div> <!-- .btn__row -->

						<div class="btn__row">
							<a href="#" class="btn btn--s btn--blue btn--w100">11</a>
						</div> <!-- .btn__row -->

						<div class="btn__row">
							<a href="#" class="btn btn--s btn--blue btn--w100">12</a>
						</div> <!-- .btn__row -->

						<div class="btn__row">
							<a href="#" class="btn btn--s btn--blue btn--w100">13</a>
						</div> <!-- .btn__row -->

						<div class="btn__row">
							<a href="#" class="btn btn--s btn--blue btn--w100">14</a>
						</div> <!-- .btn__row -->

						<div class="btn__row">
							<a href="#" class="btn btn--s btn--blue btn--w100">15</a>
						</div> <!-- .btn__row -->

						<div class="btn__row">
							<a href="#" class="btn btn--s btn--blue btn--w100">16</a>
						</div> <!-- .btn__row -->

						<div class="btn__row">
							<a href="#" class="btn btn--s btn--blue btn--w100">17</a>
						</div> <!-- .btn__row -->

					</div> <!-- .box__flors__buttons-inner -->

				</div> <!-- .box__flors__buttons -->

				<a href="#" class="btn__floor_totop" data-floor-st></a>
				<a href="#" class="btn__floor_tobottom" data-floor-sb></a>

			</div> <!-- .content__floors -->

			<div class="content__floor-view">
				<img src="tmp/floor-view-1074x680.png">
			</div> <!-- .content__floor-view -->

			<div class="content__filters">

				<div class="mb--30">

					<div class="prx">
						<p class="ft"><strong>Sprzedanych</strong></p>
						<p class="ft"><strong>mieszkań</strong></p>
						<div class="prx__value">78%</div>
						<div class="prx__bar-wrap">
							<div class="prx__bar" style="width: 78%;"></div>
						</div>
					</div> <!-- .prx -->

				</div> <!-- .m -->

				<div class="mv--30">

					<p class="ft">Start realizacji: <strong>III Kwartał 2017</strong></p>
					<p class="ft">Zakończenie realizacji: <strong>III Kwartał 2019</strong></p>

				</div> <!-- .m -->

				<div class="mv--30">

					<p class="ft">Apartamenty na piętrze: <strong>17</strong></p>
					<p class="ft font--red">Wolnych mieszkań w budynku: <strong>51</strong></p>

				</div> <!-- .m -->

				<div class="btn__row btn__row--active">
					<a href="#" class="btn btn--blue btn--wide btn--active"><div class="bo"><span class="font--bold font--l">11</span> Mieszkania wolne</div></a>
				</div> <!-- .btn__row -->

				<div class="btn__row">
					<a href="#" class="btn btn--blue btn--wide"><div class="bo"><span class="font--bold font--l">4</span> Mieszkania zarezerwowane</div></a>
				</div> <!-- .btn__row -->

				<div class="floor__legends">

					<div class="legend"><span class="legend__dot" style="background: #c0cee8;"></span>1 Pokojowe</div>
					<div class="legend"><span class="legend__dot" style="background: #d3daae;"></span>2 Pokojowe</div>
					<div class="legend"><span class="legend__dot" style="background: #fff2b9;"></span>3 Pokojowe</div>
					<div class="legend"><span class="legend__dot" style="background: #e9b05c;"></span>4 Pokojowe</div>
					<div class="legend"><span class="legend__dot" style="background: #e7d4d2;"></span>5 Pokojowe</div>
					<div class="legend"><span class="legend__dot" style="background: #8a514b;"></span>6 Pokojowe</div>

				</div> <!-- .floor__legends -->

			</div> <!-- .content__filters -->

		</div> <!-- box -- >

	</div> <!-- .content -->

</div> <!-- .wrapper -->

<?php include 'partials/footer.inc.php'; ?>
<?php include 'partials/foot.inc.php'; ?>