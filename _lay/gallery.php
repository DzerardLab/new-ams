<?php

	$page = array(
		'title' => 'galeria',
		'description' => '',
		'keywords' => '',
		'bodyclass' => '',
	);

?>

<?php include 'partials/head.inc.php'; ?>
<?php include 'partials/header.inc.php'; ?>

<div class="wrapper">

	<div class="content content--wide">

		<div class="box--relative box-heightfull">

			<div class="gallery__nav">
				<div class="gallery__nav__inner">

					<div class="btn__row">
						<a href="#" class="btn btn--blue btn--wide btn--active">Atrakcje</a>
					</div> <!-- .btn__row -->

					<div class="btn__row">
						<a href="#" class="btn btn--blue btn--wide">Okolice</a>
					</div> <!-- .btn__row -->

					<div class="btn__row">
						<a href="#" class="btn btn--blue btn--wide">Galeria</a>
					</div> <!-- .btn__row -->

				</div> <!-- .gallery__nav__inner -->
			</div> <!-- .gallery__nav -->

			<div class="gallery__content">

				<?php for ($i=0; $i < 32; $i++): ?>

					<a class="gallery__item" href="tmp/g1.jpg" data-fancybox="atrakcje" data-caption="image caption">
						<img src="tmp/g1.jpg">
					</a>

				<?php endfor; ?>

			</div> <!-- .gallery__content -->

		</div> <!-- box -- >

	</div> <!-- .content -->

</div> <!-- .wrapper -->

<?php include 'partials/footer.inc.php'; ?>
<?php include 'partials/foot.inc.php'; ?>